{
    "id": "fbda952a-d043-4607-ba5c-70eb3f043863",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oExpl",
    "eventList": [
        {
            "id": "5f12aeff-25d3-43ca-ab67-d370243a1302",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "fbda952a-d043-4607-ba5c-70eb3f043863"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
    "visible": true
}