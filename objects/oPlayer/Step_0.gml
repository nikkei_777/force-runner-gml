if (control)
{
	key_left = (keyboard_check(vk_left) or keyboard_check(ord("A")));
	key_right = keyboard_check(vk_right) or keyboard_check(ord("D"));
	jump = (keyboard_check_pressed(vk_up) or keyboard_check_pressed(ord("W")));

	var move = key_right - key_left;

	hsp = move * oPlayer.walksp;
	vsp = vsp + grv;


	if (place_meeting(x,y+1,oWall) )
	{
		jumps = oPlayer.max_jumps;
	}

	if ((jump) and (jumps>0))
	{
		jumps-=1;
		vsp = -hjump;
	}



	//Animation
	if (!place_meeting(x,y+1,oWall))
	{
		sprite_index = sPlayerJ;
		image_speed = 0;
		if (sign(vsp) > 0)
		{
			image_index = 1
		}
		else 
		{
			image_index = 0;
		}
	}
	else
	{
		image_speed = 1;
		if (hsp == 0)
		{
			sprite_index = sPlayerS;
		}
		else
		{
			sprite_index = sPlayerR;
		}
	}

	if (hsp != 0) image_xscale = sign(hsp);

	//Shooting
	if (mouse_check_button_pressed(mb_left))
	{
		if (hsp==0)
		{ 
			var bullet = instance_create_layer(x,y-15,"Bullets",oBullet);
		}
		else 
		{
			var bullet = instance_create_layer(x,y-8,"Bullets",oBullet);
		}
		bullet.speed = 15;
		if (image_xscale == 1)
		{
			bullet.direction = 0;
			bullet.image_xscale = 1;
		}
		else
		{
			bullet.direction = 180;
			bullet.image_xscale = -1;
		}
	}

	if (mouse_check_button_pressed(mb_right))
	{
		instance_create_layer(x,y,"Bullets",oGrenade);
		control = false;
		hsp = 0;
		vsp = 0;
		image_speed = 0;
	}
}

//Collision
if (place_meeting(x+hsp,y,oWall))
{
	while(!place_meeting(x+sign(hsp),y,oWall))
	{
		x = x + sign(hsp);
	}
	hsp = 0;
}
x = x + hsp;


if (place_meeting(x,y+vsp,oWall))
{
	while(!place_meeting(x,y+sign(vsp),oWall))
	{
		y = y + sign(vsp);
	}
	vsp = 0;
}
y = y + vsp;



// Bonuses
if (place_meeting(x,y,oBonusWalk)){
	bonus = oBonusWalk.bonus;
	walksp += bonus;
	audio_play_sound(snBonus,1,0);
	alarm[0] = oBonusWalk.sec*30;
	instance_destroy(oBonusWalk);
	bonus_blink(oBonusWalk, 5)
}









