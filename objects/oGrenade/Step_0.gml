if (time > 150) time = 0; gr_speed = 0;
time +=1;

if(time>30 && time<60) gr_speed = 2.5;
if(time>60 && time<90) gr_speed = 5;
if(time>90 && time<120) gr_speed = 7.5;
if(time>120 && time<150) gr_speed = 10;


if (mouse_check_button(mb_right) && !flying)
{
	direction = point_direction(x,y,mouse_x,mouse_y);
	image_angle = direction;
	instance_create_layer(x+5,y,"Bullets",oGrTime);
}

if (mouse_check_button_released(mb_right))
{
	speed = gr_speed;
	flying = true;
	oPlayer.control = true;
	instance_destroy(oGrTime);	
}

if (place_meeting(x,y,oWall))
{	
	//instance_create_depth(x,y,-10,oExpl);
	instance_destroy();	
}

if (flying)
{
	vsp = vsp + grv;
	y = y + vsp;
}