{
    "id": "6cceaff6-2b20-4334-97cd-4f1b81a94a53",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGrenade",
    "eventList": [
        {
            "id": "20576222-a598-45dd-ba5c-4079c441d187",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6cceaff6-2b20-4334-97cd-4f1b81a94a53"
        },
        {
            "id": "eaf95834-e1e2-48c0-8af2-c59f4e3e74bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6cceaff6-2b20-4334-97cd-4f1b81a94a53"
        },
        {
            "id": "0137b6ca-651e-4526-8477-2f9440b6a268",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "6cceaff6-2b20-4334-97cd-4f1b81a94a53"
        },
        {
            "id": "9611aa5b-20d7-4b07-98b6-0e6d1330d4dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5a46b575-2d1a-45ab-9c2e-ae2846a097d1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6cceaff6-2b20-4334-97cd-4f1b81a94a53"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f9aa805c-a2a4-410f-a6e6-a7c38b874d15",
    "visible": true
}