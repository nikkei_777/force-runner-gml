{
    "id": "011c17bf-9378-4156-acaa-db4924d548e4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBullet",
    "eventList": [
        {
            "id": "37775814-adb8-4ecc-a118-0f1c6fdf582c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "011c17bf-9378-4156-acaa-db4924d548e4"
        },
        {
            "id": "4dfd4fa1-ebd0-4655-8af2-32145cb7eea7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "011c17bf-9378-4156-acaa-db4924d548e4"
        },
        {
            "id": "11effafd-a0db-49ed-9ef0-3183a8424b2a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5a46b575-2d1a-45ab-9c2e-ae2846a097d1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "011c17bf-9378-4156-acaa-db4924d548e4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f5973923-6ef9-4247-946a-d99743ba14db",
    "visible": true
}