{
    "id": "c7bdf903-5d51-409f-9292-04d878f21fb8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGrTime",
    "eventList": [
        {
            "id": "2f524230-0792-4d75-a483-a7355c688a7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c7bdf903-5d51-409f-9292-04d878f21fb8"
        },
        {
            "id": "196664c0-d223-43da-b691-c8be11b508ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c7bdf903-5d51-409f-9292-04d878f21fb8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9689e18b-3ec8-42a9-91c7-f7f41e8a8bd5",
    "visible": true
}