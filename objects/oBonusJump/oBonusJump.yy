{
    "id": "089b2e25-ba04-465f-8d0b-9a29836752fc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBonusJump",
    "eventList": [
        {
            "id": "4fa00e12-f1f1-475f-bd07-6454a03257f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "089b2e25-ba04-465f-8d0b-9a29836752fc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "54505f4e-7c0d-4539-9023-9d8afa5ed31a",
    "visible": true
}