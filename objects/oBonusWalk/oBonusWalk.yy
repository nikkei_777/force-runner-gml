{
    "id": "f233d9fa-cf6a-4074-8a2f-8d8d36bc00ba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBonusWalk",
    "eventList": [
        {
            "id": "960a99b4-13d0-4395-8499-2100c47dea32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f233d9fa-cf6a-4074-8a2f-8d8d36bc00ba"
        },
        {
            "id": "a5f508ab-5ac1-4031-8a5d-0e4239768c8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f233d9fa-cf6a-4074-8a2f-8d8d36bc00ba"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7bc823dd-5f04-4482-904c-e5a79806d9c8",
    "visible": true
}