{
    "id": "fa25c4cc-ac78-461b-af14-9c44cfe15a73",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWall",
    "eventList": [
        {
            "id": "18a0acb2-ebec-4a8b-93f7-a0f70f84a637",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6cceaff6-2b20-4334-97cd-4f1b81a94a53",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "fa25c4cc-ac78-461b-af14-9c44cfe15a73"
        },
        {
            "id": "c5cadb2b-9f3b-42b1-8642-19c4eba73661",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fa25c4cc-ac78-461b-af14-9c44cfe15a73"
        },
        {
            "id": "922e62df-7d18-4772-9132-fbe191c97094",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "fa25c4cc-ac78-461b-af14-9c44cfe15a73"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7a81aeb3-0221-40e4-9fba-a59c9a844555",
    "visible": true
}