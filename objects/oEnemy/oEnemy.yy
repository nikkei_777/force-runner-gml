{
    "id": "5a46b575-2d1a-45ab-9c2e-ae2846a097d1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemy",
    "eventList": [
        {
            "id": "511ee76f-5bd5-4913-9b68-3144ccf23998",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5a46b575-2d1a-45ab-9c2e-ae2846a097d1"
        },
        {
            "id": "5a35a034-1ac6-49cd-a5ca-bfd0485110ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5a46b575-2d1a-45ab-9c2e-ae2846a097d1"
        },
        {
            "id": "ab0a7afe-b771-47f4-8d0f-6ed146e8052e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "011c17bf-9378-4156-acaa-db4924d548e4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5a46b575-2d1a-45ab-9c2e-ae2846a097d1"
        },
        {
            "id": "97508f15-8b7a-49d4-9d2e-63ef8e7ee8d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6cceaff6-2b20-4334-97cd-4f1b81a94a53",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5a46b575-2d1a-45ab-9c2e-ae2846a097d1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3eee2418-3a8d-4a2f-bb35-b87cab2ee42c",
    "visible": true
}