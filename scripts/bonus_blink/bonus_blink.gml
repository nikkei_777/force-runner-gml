var obj = argument0
var period = argument1*30

with instance_create_layer(x,y-70,"Bullets",obj) {
	image_xscale = 0.5;	
	image_yscale = 0.5;	
	alarm[0] = period;
}