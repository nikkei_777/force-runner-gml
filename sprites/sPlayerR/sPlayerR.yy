{
    "id": "730cb2e4-46d1-4933-9f01-77035e77da2a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 12,
    "bbox_right": 33,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51f36167-913c-4153-b5d7-20f637a6f6a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "730cb2e4-46d1-4933-9f01-77035e77da2a",
            "compositeImage": {
                "id": "933d5b1c-04bc-4065-a70b-a033ac20f4d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51f36167-913c-4153-b5d7-20f637a6f6a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4b2a7df-a7f2-4a72-89fc-441d76714a46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51f36167-913c-4153-b5d7-20f637a6f6a1",
                    "LayerId": "eed112c2-671d-4cb7-96d6-6feb6c90eca4"
                }
            ]
        },
        {
            "id": "9c91d7b3-3015-4a81-9b84-db88d0327acd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "730cb2e4-46d1-4933-9f01-77035e77da2a",
            "compositeImage": {
                "id": "aa653539-e050-472a-a003-f324afe0fd67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c91d7b3-3015-4a81-9b84-db88d0327acd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a16c11db-3002-4744-b818-410f4b92e2b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c91d7b3-3015-4a81-9b84-db88d0327acd",
                    "LayerId": "eed112c2-671d-4cb7-96d6-6feb6c90eca4"
                }
            ]
        },
        {
            "id": "2239ac09-0ffc-4af1-9393-0ba258c9fbfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "730cb2e4-46d1-4933-9f01-77035e77da2a",
            "compositeImage": {
                "id": "14710ba0-d4c3-4aa0-9fff-c5317100c21b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2239ac09-0ffc-4af1-9393-0ba258c9fbfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "412a71e4-92aa-4741-a219-afa1df636ad6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2239ac09-0ffc-4af1-9393-0ba258c9fbfa",
                    "LayerId": "eed112c2-671d-4cb7-96d6-6feb6c90eca4"
                }
            ]
        },
        {
            "id": "f54f5165-e623-45bc-8472-eee106d26cb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "730cb2e4-46d1-4933-9f01-77035e77da2a",
            "compositeImage": {
                "id": "da1d2829-4d8d-4cf0-82b6-96f02de13c84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f54f5165-e623-45bc-8472-eee106d26cb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9014da3-e17f-42c5-8a5f-68f0a448e64a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f54f5165-e623-45bc-8472-eee106d26cb7",
                    "LayerId": "eed112c2-671d-4cb7-96d6-6feb6c90eca4"
                }
            ]
        },
        {
            "id": "aafb97df-4983-422c-922e-d0fe2313d9bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "730cb2e4-46d1-4933-9f01-77035e77da2a",
            "compositeImage": {
                "id": "37447951-63d3-447c-bcf0-f2fb26cfb3f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aafb97df-4983-422c-922e-d0fe2313d9bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc6b2f5a-c787-40c0-9d41-20083f594e24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aafb97df-4983-422c-922e-d0fe2313d9bb",
                    "LayerId": "eed112c2-671d-4cb7-96d6-6feb6c90eca4"
                }
            ]
        },
        {
            "id": "ff159d22-8dee-4290-b442-fb865923fdb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "730cb2e4-46d1-4933-9f01-77035e77da2a",
            "compositeImage": {
                "id": "fa291f1d-3833-4c02-ab42-99647012ec24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff159d22-8dee-4290-b442-fb865923fdb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69e177cb-1c26-4c21-96a8-c86154396812",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff159d22-8dee-4290-b442-fb865923fdb2",
                    "LayerId": "eed112c2-671d-4cb7-96d6-6feb6c90eca4"
                }
            ]
        },
        {
            "id": "d5910c0f-f3c6-495b-9475-def9ee19d664",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "730cb2e4-46d1-4933-9f01-77035e77da2a",
            "compositeImage": {
                "id": "49f6509e-f19d-4c69-9b1b-0d01b8b43eca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5910c0f-f3c6-495b-9475-def9ee19d664",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "923baec2-2f18-4ee7-b103-a2000bc331e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5910c0f-f3c6-495b-9475-def9ee19d664",
                    "LayerId": "eed112c2-671d-4cb7-96d6-6feb6c90eca4"
                }
            ]
        },
        {
            "id": "78fea073-2f26-49f6-bceb-b8abeb1f4c9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "730cb2e4-46d1-4933-9f01-77035e77da2a",
            "compositeImage": {
                "id": "8b1924f3-bebe-40f2-ae05-47a6776426be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78fea073-2f26-49f6-bceb-b8abeb1f4c9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d70bb8d-b029-466a-b23f-1ec11ced1d4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78fea073-2f26-49f6-bceb-b8abeb1f4c9d",
                    "LayerId": "eed112c2-671d-4cb7-96d6-6feb6c90eca4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "eed112c2-671d-4cb7-96d6-6feb6c90eca4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "730cb2e4-46d1-4933-9f01-77035e77da2a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "cce0242d-da22-45ae-a44b-cd3ecca60dbd",
    "type": 0,
    "width": 46,
    "xorig": 23,
    "yorig": 25
}