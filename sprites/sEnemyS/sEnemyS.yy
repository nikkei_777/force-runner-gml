{
    "id": "3eee2418-3a8d-4a2f-bb35-b87cab2ee42c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyS",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e7486e6-1a2f-4df7-bd6d-a8d529abfab6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3eee2418-3a8d-4a2f-bb35-b87cab2ee42c",
            "compositeImage": {
                "id": "8ac80aac-f9a4-4fc0-9aa2-f90069d3b353",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e7486e6-1a2f-4df7-bd6d-a8d529abfab6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07c3097e-0b04-493b-9a15-4622c1904a57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e7486e6-1a2f-4df7-bd6d-a8d529abfab6",
                    "LayerId": "625ece31-fc4e-436c-b3c4-618e82ac75b5"
                },
                {
                    "id": "c71d58f6-0216-4aeb-87a5-daa7be4199f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e7486e6-1a2f-4df7-bd6d-a8d529abfab6",
                    "LayerId": "16eed4ce-e6ef-4d6e-bc44-976a91a9b230"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "625ece31-fc4e-436c-b3c4-618e82ac75b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3eee2418-3a8d-4a2f-bb35-b87cab2ee42c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "16eed4ce-e6ef-4d6e-bc44-976a91a9b230",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3eee2418-3a8d-4a2f-bb35-b87cab2ee42c",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "cce0242d-da22-45ae-a44b-cd3ecca60dbd",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}