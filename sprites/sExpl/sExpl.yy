{
    "id": "0f721430-b058-4d5d-9491-d4388ca811c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sExpl",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 10,
    "bbox_right": 53,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8e5788c-7b9f-4a0a-ab79-8be99f5267ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
            "compositeImage": {
                "id": "3385dfac-2ba7-44a6-a560-d43c387d643e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8e5788c-7b9f-4a0a-ab79-8be99f5267ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2c36a03-92fa-4bc4-be5c-6c0be97245dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8e5788c-7b9f-4a0a-ab79-8be99f5267ff",
                    "LayerId": "14d81da5-ab0b-43cd-b804-bbd33c2f2b37"
                }
            ]
        },
        {
            "id": "0018a81c-45f1-40cf-8d2e-26992d6c251b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
            "compositeImage": {
                "id": "f7ede62a-84d3-484b-86ce-81f96b2052c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0018a81c-45f1-40cf-8d2e-26992d6c251b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d71e419e-00e8-441a-8c6b-102c1d802580",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0018a81c-45f1-40cf-8d2e-26992d6c251b",
                    "LayerId": "14d81da5-ab0b-43cd-b804-bbd33c2f2b37"
                }
            ]
        },
        {
            "id": "9b68758c-c05a-4ff6-a650-f2406f1cca08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
            "compositeImage": {
                "id": "95a8c299-cf69-4e5c-9dae-b371be7a6398",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b68758c-c05a-4ff6-a650-f2406f1cca08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a86bb80-2dda-4a87-8048-1367206f5765",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b68758c-c05a-4ff6-a650-f2406f1cca08",
                    "LayerId": "14d81da5-ab0b-43cd-b804-bbd33c2f2b37"
                }
            ]
        },
        {
            "id": "50213058-1fd0-466b-b11a-65d4c1662a1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
            "compositeImage": {
                "id": "4eff7508-af23-4273-80da-dfcc45cae366",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50213058-1fd0-466b-b11a-65d4c1662a1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f74704f-0ec0-4fbb-8838-384a789bac09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50213058-1fd0-466b-b11a-65d4c1662a1d",
                    "LayerId": "14d81da5-ab0b-43cd-b804-bbd33c2f2b37"
                }
            ]
        },
        {
            "id": "36af1d40-384d-46aa-b36e-ab589656d755",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
            "compositeImage": {
                "id": "c42c0784-aad7-45fa-9ab2-bd3982062730",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36af1d40-384d-46aa-b36e-ab589656d755",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25f3bac3-5ec0-4c8a-abc3-198a3b7782c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36af1d40-384d-46aa-b36e-ab589656d755",
                    "LayerId": "14d81da5-ab0b-43cd-b804-bbd33c2f2b37"
                }
            ]
        },
        {
            "id": "3e6448ce-587b-4e51-95e1-733f087b7bb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
            "compositeImage": {
                "id": "ab7bf08f-9767-4c4a-8883-fb5ce4e21e54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e6448ce-587b-4e51-95e1-733f087b7bb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac625956-2c86-40d9-ba40-9d46b383a874",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e6448ce-587b-4e51-95e1-733f087b7bb9",
                    "LayerId": "14d81da5-ab0b-43cd-b804-bbd33c2f2b37"
                }
            ]
        },
        {
            "id": "8dacfdf8-cc28-41e2-bc87-29d6e1bf4f43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
            "compositeImage": {
                "id": "79a8372f-02cb-44ce-96be-4fb7b273e09f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dacfdf8-cc28-41e2-bc87-29d6e1bf4f43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae34529c-f1ff-4f7a-9c1d-5456c46a641c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dacfdf8-cc28-41e2-bc87-29d6e1bf4f43",
                    "LayerId": "14d81da5-ab0b-43cd-b804-bbd33c2f2b37"
                }
            ]
        },
        {
            "id": "4b32d5e7-211c-45e7-af55-34c9420f41cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
            "compositeImage": {
                "id": "08b663b8-3cd1-48e7-a854-4fe3aa53a416",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b32d5e7-211c-45e7-af55-34c9420f41cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "747054ee-e468-421d-be5c-526d7906d910",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b32d5e7-211c-45e7-af55-34c9420f41cf",
                    "LayerId": "14d81da5-ab0b-43cd-b804-bbd33c2f2b37"
                }
            ]
        },
        {
            "id": "646b3916-aeb2-4152-8495-a4fb54714069",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
            "compositeImage": {
                "id": "b8054a5e-e640-4413-b165-e17572489666",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "646b3916-aeb2-4152-8495-a4fb54714069",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "593bc31d-a0af-4e55-ae41-4e8d0379e807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "646b3916-aeb2-4152-8495-a4fb54714069",
                    "LayerId": "14d81da5-ab0b-43cd-b804-bbd33c2f2b37"
                }
            ]
        },
        {
            "id": "4f18a7fd-9611-40b3-990c-0c9bcf64b7aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
            "compositeImage": {
                "id": "8edaeb3b-3b9d-4a62-b41c-f821c40916ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f18a7fd-9611-40b3-990c-0c9bcf64b7aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8130d474-d014-47fa-8c3c-c4b936a38c25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f18a7fd-9611-40b3-990c-0c9bcf64b7aa",
                    "LayerId": "14d81da5-ab0b-43cd-b804-bbd33c2f2b37"
                }
            ]
        },
        {
            "id": "e41d41a7-96e0-47f9-bbdf-cd23c4d7f269",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
            "compositeImage": {
                "id": "82a8cbf8-c91f-4e8b-90fd-9f30e05235e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e41d41a7-96e0-47f9-bbdf-cd23c4d7f269",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b97ee28-a513-406c-8442-e940550ceb78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e41d41a7-96e0-47f9-bbdf-cd23c4d7f269",
                    "LayerId": "14d81da5-ab0b-43cd-b804-bbd33c2f2b37"
                }
            ]
        },
        {
            "id": "62268be6-cd99-4ec3-8689-275823cfc30e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
            "compositeImage": {
                "id": "9ef2b1de-bac2-453a-90d7-f664ac3d2af2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62268be6-cd99-4ec3-8689-275823cfc30e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c559f4b6-bee9-4e06-87ae-0a074df631fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62268be6-cd99-4ec3-8689-275823cfc30e",
                    "LayerId": "14d81da5-ab0b-43cd-b804-bbd33c2f2b37"
                }
            ]
        },
        {
            "id": "3d30de58-12fc-44a1-9863-7b779fe0d517",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
            "compositeImage": {
                "id": "281497a7-c76b-4aab-92f9-8d03b1aad2f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d30de58-12fc-44a1-9863-7b779fe0d517",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e5b1cf9-f7a3-4ed8-8dfe-20be09be6cce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d30de58-12fc-44a1-9863-7b779fe0d517",
                    "LayerId": "14d81da5-ab0b-43cd-b804-bbd33c2f2b37"
                }
            ]
        },
        {
            "id": "f5cc2bc2-deea-4326-86cf-967fbda280d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
            "compositeImage": {
                "id": "b0394d79-0a29-4c76-881f-b9a90e22812b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5cc2bc2-deea-4326-86cf-967fbda280d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0ba70c6-e219-4fdd-b301-63a77dd9bf90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5cc2bc2-deea-4326-86cf-967fbda280d6",
                    "LayerId": "14d81da5-ab0b-43cd-b804-bbd33c2f2b37"
                }
            ]
        },
        {
            "id": "670a98d4-cd43-42b2-901e-6c933785f112",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
            "compositeImage": {
                "id": "c6f146a4-f1b3-4457-b6f4-229ec5da194b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "670a98d4-cd43-42b2-901e-6c933785f112",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7c99821-3783-48e0-98f5-b6d16f4828be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "670a98d4-cd43-42b2-901e-6c933785f112",
                    "LayerId": "14d81da5-ab0b-43cd-b804-bbd33c2f2b37"
                }
            ]
        },
        {
            "id": "b548b056-bbb0-4537-bfa2-74a86d375b84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
            "compositeImage": {
                "id": "9d29d901-719e-41e3-93a1-2bc24301c47a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b548b056-bbb0-4537-bfa2-74a86d375b84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4011f0e5-a887-4300-a659-22d17677bcb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b548b056-bbb0-4537-bfa2-74a86d375b84",
                    "LayerId": "14d81da5-ab0b-43cd-b804-bbd33c2f2b37"
                }
            ]
        },
        {
            "id": "6982f462-2c60-49a2-af12-e636857f63da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
            "compositeImage": {
                "id": "5932bba0-3b98-4e53-8ab4-412b39c7d2b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6982f462-2c60-49a2-af12-e636857f63da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f703b5d5-718e-4462-8419-c29c52975d7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6982f462-2c60-49a2-af12-e636857f63da",
                    "LayerId": "14d81da5-ab0b-43cd-b804-bbd33c2f2b37"
                }
            ]
        },
        {
            "id": "d5627d8f-2e54-4a07-a178-c4df4a77b6da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
            "compositeImage": {
                "id": "66526dda-e2a0-42f4-b026-5d06e2bfc868",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5627d8f-2e54-4a07-a178-c4df4a77b6da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e422628-643b-4cac-aaee-f40978ae204d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5627d8f-2e54-4a07-a178-c4df4a77b6da",
                    "LayerId": "14d81da5-ab0b-43cd-b804-bbd33c2f2b37"
                }
            ]
        },
        {
            "id": "94370204-fbf2-40a3-b54b-e378756629ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
            "compositeImage": {
                "id": "206d6124-238e-44ee-9168-563c8bacfc82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94370204-fbf2-40a3-b54b-e378756629ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52238666-397b-4384-bee6-b05cccbc9771",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94370204-fbf2-40a3-b54b-e378756629ee",
                    "LayerId": "14d81da5-ab0b-43cd-b804-bbd33c2f2b37"
                }
            ]
        },
        {
            "id": "254e764e-4ea9-490a-923f-b30b28893271",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
            "compositeImage": {
                "id": "fbd5dc47-ca61-40b6-86a8-010ecfb698e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "254e764e-4ea9-490a-923f-b30b28893271",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb6a8bff-614d-4e9b-ada0-44564d8fa0be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "254e764e-4ea9-490a-923f-b30b28893271",
                    "LayerId": "14d81da5-ab0b-43cd-b804-bbd33c2f2b37"
                }
            ]
        },
        {
            "id": "31e2919b-609c-4ff6-8014-95404e2647b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
            "compositeImage": {
                "id": "39fbd6d6-9231-4766-8619-91a385223074",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31e2919b-609c-4ff6-8014-95404e2647b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "845593ea-a66d-48bc-9a21-68de6f994c2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31e2919b-609c-4ff6-8014-95404e2647b4",
                    "LayerId": "14d81da5-ab0b-43cd-b804-bbd33c2f2b37"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "14d81da5-ab0b-43cd-b804-bbd33c2f2b37",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f721430-b058-4d5d-9491-d4388ca811c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "cce0242d-da22-45ae-a44b-cd3ecca60dbd",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}