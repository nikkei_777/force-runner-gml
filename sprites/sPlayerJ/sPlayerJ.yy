{
    "id": "6b08886d-955d-473f-b3cc-a72ad2368509",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerJ",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 12,
    "bbox_right": 33,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ba8bbdf-71c5-4950-ba39-1267df7f9b4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b08886d-955d-473f-b3cc-a72ad2368509",
            "compositeImage": {
                "id": "7b672613-aab4-4a21-852c-854938f6d6d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ba8bbdf-71c5-4950-ba39-1267df7f9b4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae189bfd-b13f-479b-9aae-da6ddc872ec6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ba8bbdf-71c5-4950-ba39-1267df7f9b4f",
                    "LayerId": "60e29978-c3c7-4449-ae42-b767d32de57e"
                }
            ]
        },
        {
            "id": "b0170a43-2fb5-4a2a-9e5f-234a2d3b3b74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b08886d-955d-473f-b3cc-a72ad2368509",
            "compositeImage": {
                "id": "3b021ac9-6603-4c97-a32c-258abe307c20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0170a43-2fb5-4a2a-9e5f-234a2d3b3b74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "745e8960-31f9-483c-8fcb-03a1a6154f86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0170a43-2fb5-4a2a-9e5f-234a2d3b3b74",
                    "LayerId": "60e29978-c3c7-4449-ae42-b767d32de57e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "60e29978-c3c7-4449-ae42-b767d32de57e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b08886d-955d-473f-b3cc-a72ad2368509",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "cce0242d-da22-45ae-a44b-cd3ecca60dbd",
    "type": 0,
    "width": 46,
    "xorig": 23,
    "yorig": 25
}