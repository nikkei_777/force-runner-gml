{
    "id": "f9aa805c-a2a4-410f-a6e6-a7c38b874d15",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGrenade",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 7,
    "bbox_right": 18,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d5404290-fb0f-44ac-8c63-e40b4558d18b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9aa805c-a2a4-410f-a6e6-a7c38b874d15",
            "compositeImage": {
                "id": "ea53163f-4e73-4eab-801d-8a76b62a0a11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5404290-fb0f-44ac-8c63-e40b4558d18b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7ffe998-fa1b-4bbd-a6dc-9497e99abdfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5404290-fb0f-44ac-8c63-e40b4558d18b",
                    "LayerId": "1db65e27-2212-43ad-a2d4-d259e79575af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "1db65e27-2212-43ad-a2d4-d259e79575af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9aa805c-a2a4-410f-a6e6-a7c38b874d15",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "cce0242d-da22-45ae-a44b-cd3ecca60dbd",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}