{
    "id": "7bc823dd-5f04-4482-904c-e5a79806d9c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBonusWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 3,
    "bbox_right": 27,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7afba7be-749e-4bdb-9d44-17a0a4845393",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bc823dd-5f04-4482-904c-e5a79806d9c8",
            "compositeImage": {
                "id": "0c2e440e-0c21-4736-b000-36ee8c101efb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7afba7be-749e-4bdb-9d44-17a0a4845393",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1e6cf84-679e-4dcb-b157-06bc588cbeee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7afba7be-749e-4bdb-9d44-17a0a4845393",
                    "LayerId": "8e604d81-a161-41c4-bf1c-e6322467b808"
                },
                {
                    "id": "4e9802c0-7687-40c6-833d-b3dec1edf29a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7afba7be-749e-4bdb-9d44-17a0a4845393",
                    "LayerId": "7de8b4cd-48f7-456e-8b1a-83445dea32f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7de8b4cd-48f7-456e-8b1a-83445dea32f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7bc823dd-5f04-4482-904c-e5a79806d9c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "8e604d81-a161-41c4-bf1c-e6322467b808",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7bc823dd-5f04-4482-904c-e5a79806d9c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "cce0242d-da22-45ae-a44b-cd3ecca60dbd",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}