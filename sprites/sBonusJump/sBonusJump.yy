{
    "id": "54505f4e-7c0d-4539-9023-9d8afa5ed31a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBonusJump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 3,
    "bbox_right": 27,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b7790813-3b5d-407a-965f-ce6a0e4b6521",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54505f4e-7c0d-4539-9023-9d8afa5ed31a",
            "compositeImage": {
                "id": "75d2b187-f003-4b65-972a-a735dd880e22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7790813-3b5d-407a-965f-ce6a0e4b6521",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38cb0263-bc49-4d0e-b7bb-1f931489ef1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7790813-3b5d-407a-965f-ce6a0e4b6521",
                    "LayerId": "30a49f42-07e8-4177-adc8-d2b113957337"
                },
                {
                    "id": "c47e66e7-50b4-493c-8091-684655c2f486",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7790813-3b5d-407a-965f-ce6a0e4b6521",
                    "LayerId": "ba4388b5-5a8e-4d95-a882-b5cb1d711db6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ba4388b5-5a8e-4d95-a882-b5cb1d711db6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54505f4e-7c0d-4539-9023-9d8afa5ed31a",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "30a49f42-07e8-4177-adc8-d2b113957337",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54505f4e-7c0d-4539-9023-9d8afa5ed31a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "cce0242d-da22-45ae-a44b-cd3ecca60dbd",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}