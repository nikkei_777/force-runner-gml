{
    "id": "cc182738-4795-4a73-8c88-f4575e0f923e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerS",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 12,
    "bbox_right": 33,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0667ae8e-cb2b-417d-b5fc-8134dd3c10c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc182738-4795-4a73-8c88-f4575e0f923e",
            "compositeImage": {
                "id": "5fd58a99-ba24-447a-85d4-2a766c2e3d28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0667ae8e-cb2b-417d-b5fc-8134dd3c10c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3880fedc-31ed-4050-8e52-5f32848ca89e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0667ae8e-cb2b-417d-b5fc-8134dd3c10c2",
                    "LayerId": "87556d8f-372e-47cb-80ba-d4f430eeff40"
                }
            ]
        },
        {
            "id": "d9d19c4d-17eb-41eb-b9a6-3bd05214e3b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc182738-4795-4a73-8c88-f4575e0f923e",
            "compositeImage": {
                "id": "ec53383a-0b38-48a8-9352-ff71f2f74543",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9d19c4d-17eb-41eb-b9a6-3bd05214e3b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67dccfbc-c811-48ac-96cb-930232d15d84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9d19c4d-17eb-41eb-b9a6-3bd05214e3b0",
                    "LayerId": "87556d8f-372e-47cb-80ba-d4f430eeff40"
                }
            ]
        },
        {
            "id": "82a7c561-fb69-46db-8670-5d543030a044",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc182738-4795-4a73-8c88-f4575e0f923e",
            "compositeImage": {
                "id": "850340b2-b320-4a6c-8c17-17ea45d99352",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82a7c561-fb69-46db-8670-5d543030a044",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84f41233-e913-4626-94cd-26835b506575",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82a7c561-fb69-46db-8670-5d543030a044",
                    "LayerId": "87556d8f-372e-47cb-80ba-d4f430eeff40"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "87556d8f-372e-47cb-80ba-d4f430eeff40",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc182738-4795-4a73-8c88-f4575e0f923e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "cce0242d-da22-45ae-a44b-cd3ecca60dbd",
    "type": 0,
    "width": 46,
    "xorig": 23,
    "yorig": 25
}