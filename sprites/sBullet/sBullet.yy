{
    "id": "f5973923-6ef9-4247-946a-d99743ba14db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76be253f-2786-40c2-a78e-b8b7087c7f10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5973923-6ef9-4247-946a-d99743ba14db",
            "compositeImage": {
                "id": "f0ab7c20-675a-4d66-88a6-026ec084e523",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76be253f-2786-40c2-a78e-b8b7087c7f10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efcde2ce-1734-4c56-abdc-a76ed57b6f38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76be253f-2786-40c2-a78e-b8b7087c7f10",
                    "LayerId": "48f2bf5e-873e-4773-9f0c-7c6b8dd7f495"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "48f2bf5e-873e-4773-9f0c-7c6b8dd7f495",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5973923-6ef9-4247-946a-d99743ba14db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "cce0242d-da22-45ae-a44b-cd3ecca60dbd",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 1
}