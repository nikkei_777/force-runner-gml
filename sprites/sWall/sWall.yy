{
    "id": "7a81aeb3-0221-40e4-9fba-a59c9a844555",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "83ef76e2-c793-452a-a0a5-f74f8c2eaf50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a81aeb3-0221-40e4-9fba-a59c9a844555",
            "compositeImage": {
                "id": "b75cd5fd-42cc-4638-9945-f8bbd6bde8a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83ef76e2-c793-452a-a0a5-f74f8c2eaf50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "026863ee-982c-4373-ae18-cf80da8ffd8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83ef76e2-c793-452a-a0a5-f74f8c2eaf50",
                    "LayerId": "24d782e7-e35f-48cb-b000-4d599f9f96fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "24d782e7-e35f-48cb-b000-4d599f9f96fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a81aeb3-0221-40e4-9fba-a59c9a844555",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "cce0242d-da22-45ae-a44b-cd3ecca60dbd",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}