{
    "id": "9689e18b-3ec8-42a9-91c7-f7f41e8a8bd5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGrTime",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 47,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a87d0335-ddac-47f5-9e68-d88278ac3de5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9689e18b-3ec8-42a9-91c7-f7f41e8a8bd5",
            "compositeImage": {
                "id": "ad58309e-a488-4d03-bff3-3f3db5000097",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a87d0335-ddac-47f5-9e68-d88278ac3de5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3b8e794-831c-421c-9005-a91725a453b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a87d0335-ddac-47f5-9e68-d88278ac3de5",
                    "LayerId": "5cea96ad-c4af-4d72-aa6e-a81d75d7a382"
                }
            ]
        },
        {
            "id": "319aa469-47f9-4251-bbd0-7f8b63170ca6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9689e18b-3ec8-42a9-91c7-f7f41e8a8bd5",
            "compositeImage": {
                "id": "70319d90-baaf-4bf2-96a4-947d1a50b443",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "319aa469-47f9-4251-bbd0-7f8b63170ca6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ebd9d77-4516-48b7-928a-944d77876ad5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "319aa469-47f9-4251-bbd0-7f8b63170ca6",
                    "LayerId": "5cea96ad-c4af-4d72-aa6e-a81d75d7a382"
                }
            ]
        },
        {
            "id": "71967c4b-11ec-4b18-9e9d-216f783ad032",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9689e18b-3ec8-42a9-91c7-f7f41e8a8bd5",
            "compositeImage": {
                "id": "5e5c52ce-71f4-486a-98e1-588932a90828",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71967c4b-11ec-4b18-9e9d-216f783ad032",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "814d6315-51d3-4da2-a248-5ea86a4e5504",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71967c4b-11ec-4b18-9e9d-216f783ad032",
                    "LayerId": "5cea96ad-c4af-4d72-aa6e-a81d75d7a382"
                }
            ]
        },
        {
            "id": "3e0b23a5-5146-4730-9cca-f8cb901672a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9689e18b-3ec8-42a9-91c7-f7f41e8a8bd5",
            "compositeImage": {
                "id": "8ea0005c-c39e-4e74-aaed-731b0a5bca63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e0b23a5-5146-4730-9cca-f8cb901672a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "358fd3c5-06fe-4d59-866a-7165c517905d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e0b23a5-5146-4730-9cca-f8cb901672a1",
                    "LayerId": "5cea96ad-c4af-4d72-aa6e-a81d75d7a382"
                }
            ]
        },
        {
            "id": "184cd343-749f-47c3-ae7a-b35f84aaf00e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9689e18b-3ec8-42a9-91c7-f7f41e8a8bd5",
            "compositeImage": {
                "id": "d9e90f53-597d-4f37-9c17-ff22fad22ef1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "184cd343-749f-47c3-ae7a-b35f84aaf00e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8edd1b6e-28aa-4a94-8f7e-5a2e911f37c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "184cd343-749f-47c3-ae7a-b35f84aaf00e",
                    "LayerId": "5cea96ad-c4af-4d72-aa6e-a81d75d7a382"
                }
            ]
        },
        {
            "id": "5ad46775-f4a7-4ff7-9f99-718698c0decb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9689e18b-3ec8-42a9-91c7-f7f41e8a8bd5",
            "compositeImage": {
                "id": "42ef6439-bc45-4a35-9026-826a09aecc15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ad46775-f4a7-4ff7-9f99-718698c0decb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c322e9f6-89e0-47cd-833a-2d80343a73fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ad46775-f4a7-4ff7-9f99-718698c0decb",
                    "LayerId": "5cea96ad-c4af-4d72-aa6e-a81d75d7a382"
                }
            ]
        },
        {
            "id": "2c9be876-9322-4715-a60e-690c793efbf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9689e18b-3ec8-42a9-91c7-f7f41e8a8bd5",
            "compositeImage": {
                "id": "31dbb782-2419-4828-8f97-eaa0d89b6963",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c9be876-9322-4715-a60e-690c793efbf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1714d769-3346-4573-8618-804dc39de8a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c9be876-9322-4715-a60e-690c793efbf5",
                    "LayerId": "5cea96ad-c4af-4d72-aa6e-a81d75d7a382"
                }
            ]
        },
        {
            "id": "11cdf180-1c82-4469-b5c2-6aecca18634b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9689e18b-3ec8-42a9-91c7-f7f41e8a8bd5",
            "compositeImage": {
                "id": "0c68f48f-1a64-4733-988e-30b8d36d39c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11cdf180-1c82-4469-b5c2-6aecca18634b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5201ff3-ba9f-46ac-94de-7add9d5876b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11cdf180-1c82-4469-b5c2-6aecca18634b",
                    "LayerId": "5cea96ad-c4af-4d72-aa6e-a81d75d7a382"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "5cea96ad-c4af-4d72-aa6e-a81d75d7a382",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9689e18b-3ec8-42a9-91c7-f7f41e8a8bd5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "cce0242d-da22-45ae-a44b-cd3ecca60dbd",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 12
}